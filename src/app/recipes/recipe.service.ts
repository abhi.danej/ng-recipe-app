import { Recipe } from "./recipe.model";
import { Injectable } from "@angular/core";
import { Ingredient } from "../shared/ingredient.model";
import { ShoppingListService } from "../shopping-list/shoppinglist.service";

@Injectable()
export class RecipeService {
  private recipes: Recipe[] = [
    new Recipe(
      "Meat Schnitzel",
      "This is a simple recipe",
      "https://www.bing.com/th?id=OIP.hzA-J_EKcKVt5wWRfMxWRAHaE8&pid=Api&rs=1&p=0",
      [new Ingredient("Meat", 1), new Ingredient("Bread", 3)]
    ),
    new Recipe(
      "Health Salad",
      "This is a simple recipe",
      "https://www.bing.com/th?id=OIP.vGlEuf5WxAwParc4N4s9xQHaF_&pid=Api&rs=1&p=0",
      [new Ingredient("Veggie", 1), new Ingredient("Oil", 3)]
    )
  ];

  getRecipes() {
    return this.recipes.slice();
  }

  addIngToShoppingList(ings: Ingredient[]) {
    this.slService.addIngredients(ings);
  }

  getRecipeById(id: number) {
    return this.recipes.slice()[id];
  }

  constructor(private slService: ShoppingListService) {}
}
