import { Component } from "@angular/core";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent {
  title = "ng-recipe-app";
  flag: number = 1;

  onMenuEvent(flag: number) {
    this.flag = flag;
  }
}
